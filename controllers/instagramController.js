/**
 *Instagram Controller used for entire logic regarding the instagram
 * Author Semir Hodzic
 */
// We use axios for HTTP calls
const axios = require('axios');
/**
 * Redirect to view index with form
 * @param req
 * @param res
 */
exports.index = function (req, res) {
    res.status(200).render('index', {title: 'Enter Instagram handle'});
};

/**
 * * We use Foo as RegExp to get all the JSON;
 * We retrieve the 2° item ([1]) of the returned matches from .match() JS function, since the 1° is the whole contains also our "delimitator";
 * We delete the last character of our object because, in the source page, it ends with ; and, of course, it won't be a valid JSON.
 * We map useful information into two vars : profileData and posts and send that to view
 * @param req
 * @param res
 */
exports.scrapData = function (req, res) {
    // array for posts
    let posts = [];
    // Profile data object
    let profileData = {
        name: '',
        description: '',
        followers_number: '',
        following_number: ''
    };
    axios.get('https://www.instagram.com/' + req.body.instagram_handle + '/')
        .then(function (response) {
            /**
             * @type {string}
             */
            const jsonObject = response.data.match(/<script type="text\/javascript">window\._sharedData = (.*)<\/script>/)[1].slice(0, -1);
            const userInfo = JSON.parse(jsonObject);
            // map profile page info
            profileData.name = userInfo.entry_data.ProfilePage[0].graphql.user.full_name;
            profileData.description = userInfo.entry_data.ProfilePage[0].graphql.user.biography;
            profileData.followers_number = userInfo.entry_data.ProfilePage[0].graphql.user.edge_followed_by.count;
            profileData.following_number = userInfo.entry_data.ProfilePage[0].graphql.user.edge_follow.count;
            // Retrieve only the first 6 results
            const mediaArray = userInfo.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges.splice(0, 6)
            console.log(posts);
            for (let media of mediaArray) {
                const node = media.node;
                // Process only if is an image
                if ((node.__typename && node.__typename !== 'GraphImage')) {
                    continue
                }
                // Push the thumbnail src in the array
                posts.push({id: node.id, url: node.thumbnail_src})
            }
            res.status(200).render('profile', {profileData: profileData, posts: posts});

        })
        .catch(function (error) {
            console.log(error.Error);
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                res.status(200).render('error', {title: 'Enter Instagram handle', message: 'That instagram handle do not exist please try another one'});
                // res.status(500).json({error: 'That instagram handle do not exist please try another one'});
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                res.status(500).json({error: error.request});
            } else {
                // Something happened in setting up the request that triggered an Error
                res.status(500).json({error: error.message});
            }
        });
};