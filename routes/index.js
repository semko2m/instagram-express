/**
 *Index Routing  used base routes
 * Author Semir Hodzic
 */

const express = require('express');
const router = express.Router();

const instagram_handle = require('../controllers/instagramController');

// GET catalog home page.
router.get('/', instagram_handle.index);
router.post('/profile-data', instagram_handle.scrapData);


module.exports = router;
